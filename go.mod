module example.com/test

go 1.14

require (
	github.com/joho/godotenv v1.3.0
	github.com/namsral/flag v1.7.4-pre
	github.com/rs/zerolog v1.20.0
	github.com/satori/go.uuid v1.2.0
	github.com/segmentio/kafka-go v0.4.9
	github.com/tarantool/go-tarantool v0.0.0-20201220111423-77ce7d9a407a
)
