package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
	uuid "github.com/satori/go.uuid"
	"github.com/segmentio/kafka-go"
	_ "github.com/segmentio/kafka-go/snappy"
	"github.com/tarantool/go-tarantool"
)

type Event struct {
	UserID    int    `json:"id"`
	Name      string `json:"type"`
	Text      string `json:"text"`
	Timestamp int64  `json:"timestamp"`
}

func NewTarantoolConn(user, address string) *tarantool.Connection {
	opts := tarantool.Opts{User: user, Reconnect: 10 * time.Second, MaxReconnects: 20}
	conn, err := tarantool.Connect(address, opts)
	if err != nil {
		panic(err)
	}
	return conn
}

func main() {
	godotenv.Load(".env")
	var client = NewTarantoolConn(os.Getenv("TARANTOOL_NAME"), os.Getenv("TARANTOOL_URL"))

	config := kafka.ReaderConfig{
		Brokers:         []string{os.Getenv("KAFKA_URL")},
		GroupID:         os.Getenv("KAFKA_CLIENT_ID"),
		Topic:           os.Getenv("KAFKA_TOPIC"),
		MinBytes:        10e3,
		MaxBytes:        10e6,
		MaxWait:         1 * time.Second,
		ReadLagInterval: -1,
	}

	reader := kafka.NewReader(config)

	defer reader.Close()

	for {
		m, err := reader.ReadMessage(context.Background())
		if err != nil {
			log.Error().Msgf("error while receiving message: %s", err.Error())
		}
		event := Event{}
		json.Unmarshal(m.Value, &event)
		newsID := uuid.NewV4().String()
		resp, err := client.Call("update_feed", []interface{}{event.UserID, newsID, event.Text, event.Timestamp})
		fmt.Printf("message at offset %d: %s = %s\n", resp, err, string(m.Value))
	}

}
